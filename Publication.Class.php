<?php

abstract class Publication
{
    public static function create(PDO $pdo,$id)
    {
        $sql = 'SELECT * FROM news where id = :id';
        $data = $pdo->prepare($sql);
        $data->execute(array(':id' => $id));
        $row = $data->fetchObject();

        return $row;
    }

}
