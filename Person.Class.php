<?php
abstract class Person implements ActiveUser{
    public $name;
    public $mail;
    public $phone;
    public $role;
    public function getVisitCard(){
       return $this->name.' '.$this->mail.' '.$this->phone.' '.$this->role;
    }
    public static function getUser(PDO $pdo){
        $sql = 'SELECT * FROM users WHERE id>0';
        $data = $pdo->prepare($sql);
        $data->execute();
        $row = $data->fetchAll(PDO::FETCH_CLASS);
        
        foreach ($row as $user) {
            if ($user->role == 'Student'){
                $userData[] = new Student(
                    $user->name, 
                    $user->phone, 
                    $user->mail, 
                    $user->role, 
                    json_decode($user->avmarks,true), 
                    json_decode($user->visits,true));

            }elseif($user->role == 'Teacher'){
                    $userData[] = new Teacher(
                    $user->name, 
                    $user->phone, 
                    $user->mail, 
                    $user->role, 
                    $user->subject); 
                    

            }elseif($user->role == 'Admin'){
                    $userData[] = new Admin(
                    $user->name, 
                    $user->phone, 
                    $user->mail, 
                    $user->role, 
                    json_decode($user->workdays,true));          

            }

        }
        return  $userData;
    }
    public function __construct($name,$mail,$phone,$role){
        $this->name=$name;
        $this->mail=$mail;
        $this->phone=$phone;
        $this->role=$role;
    }
}

